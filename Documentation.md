## Qod project (Netdesigner)
## Clients
 Fibre Consultants
## Purpose 
Detecting mistake & Notification system
## Technologies
- Python 
- Gitlab
- Power Automate
- Power Bi


## WorkFlow
- Getting data from SharePoint 
- Detecting mistakes 
-  Saving results in SharePoint 
-  Notifying each Consultants 

## Author 
Mohamed_Amine_Ayer (RPA intern)